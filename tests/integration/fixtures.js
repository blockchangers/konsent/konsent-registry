const getValidOrganization = () => {
  return {
    address: "0xF2d7e3D1d51Ad442357221377cE462ba1B350a59",
    callbackUrl: "http://localhost:8083/api/v1/claims",
    did: "did:ethr:0xF2d7e3D1d51Ad442357221377cE462ba1B350a59",
    name: "test",
    domain: "blockchangers.com",
    organizationNumber: "915772137",
    publicKey: "0xd5da7cf3e57b2dff4381d66b273ee2e08d2f49a09d9ed6f9065568ad28910a7e917070fa033bec69d51f1109faa222747bd7c425368b44a29572bce4d7f01520"
  };
}


module.exports = {
  getValidOrganization
};
