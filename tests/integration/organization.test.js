const mongoose        = require('mongoose');
const test            = require('tape');
const request         = require('supertest');
const app             = require('../../registry');
const fixtures        = require('./fixtures');
const agent           = request.agent(app);

const Organization = mongoose.model('Organization');

const clean = async(t) => Organization.remove({}, (err) => t.end());

const setup = () => fixtures.getValidOrganization();

test('clean up',clean);

test('create user', async t => {
  const fixture = setup();
  const organization = new Organization(fixture);
  return await organization.save(t.end);
});

test('should return organizations', t => {
  request(app)
  .get('/api/v1/organizations')
  .expect(200)
  .end(async err => {
    const count = await Organization.countDocuments().exec();
    t.ifError(err);
    t.same(count, 1, 'number of organizations 1');
    t.end();
  });
});

test('clean up', clean);

test.onFinish(() => process.exit(0));
