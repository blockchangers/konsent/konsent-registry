const mongoose = require('mongoose');

const organizationSchema = new mongoose.Schema({
  address: {type: String, required: true},
  callbackUrl: {type: String, required: true},
  did: {type: String, required: true},
  name: {type: String, required: true},
  domain: {type: String, required: true},
  organizationNumber: {type: String, required: true},
  publicKey: {type: String, required: true},
  created: {type: Number, required: true}
});

mongoose.model('Organization', organizationSchema);
