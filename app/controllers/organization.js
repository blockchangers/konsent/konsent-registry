const mongoose                = require('mongoose');
const { logger }              = require('../utils/logger');
const { isValidatePublicKey } = require('../utils/validation');

const Organization = mongoose.model('Organization');

exports.getAll = (async(req, res) => {
  try {
    logger.info('receiving get request, tryingt to fetch organization list');
    const organizations = await Organization.find({}, { '_id': 0, '__v': 0});
    res.json(organizations);
  } catch(err) {
    logger.error(err);
    res.status(404).send('cant lookup organizations in database');
  }
});

exports.deleteSingle = (async(req, res) => {
  try {
    const did = req.params.did
    if(!did) return res.status(400).send();
    logger.info('receiving delete request, tryingt to delete entry from registry');
    const organization = await Organization.deleteMany({ did }, { '_id': 0, '__v': 0});
    res.json(organization);
  } catch (error) {
    logger.error(err);
    res.status(404).send(`cant lookup organization with did: ${did} in database`);
  }

});

exports.getSingle = (async(req, res) => {
  try {
    const did = req.params.did
    if(!did) return res.status(400).send();
    logger.info('receiving get request, tryingt to single organization');
    const organization = await Organization.findOne({ did }, { '_id': 0, '__v': 0});
    res.json(organization);
  } catch (error) {
    logger.error(err);
    res.status(404).send(`cant lookup organization with did: ${did} in database`);
  }
});

exports.post = (async(req, res) => {
  try {
    logger.info('receiving post request, trying to persist organization');
    const organizationToPersist = req.body;
    if(!isValidatePublicKey(organizationToPersist.publicKey)) return res.status(400).send('cannot persist organization');
    const organization = new Organization({...organizationToPersist, created: new Date().getTime()});
    const result = await organization.save();
    res.json(result);
  } catch (error) {
    console.error(error);
    res.status(500).send('cant save organization to database');
  }
});
