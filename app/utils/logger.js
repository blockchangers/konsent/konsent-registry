require('dotenv').config();

const logdnaWinston = require('logdna-winston');
const winston = require('winston');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;


const konsentFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = createLogger({
  level: 'info',
  format: format.json(),
  transports: [
    new transports.File({ filename: './logs/error.log', level: 'error' }),
    new transports.File({ filename: './logs/combined.log' })
  ]
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new logdnaWinston({
    key: '526e370aa512cebc3cc1934a6bab443c',
    app: 'konsent-registry',
    env: 'production',
    index_meta: true,
    handleExceptions: true
  }));
}

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      label({ label: 'konsent-registry' }),
      timestamp(),
      konsentFormat
    )
  }));
};

const messageLogger = (title, message) => {
  const wrapTitle = title ? ` \n ${title} \n ${'-'.repeat(60)}` : ''
  const wrapMessage = `\n ${'-'.repeat(60)} ${wrapTitle} \n`
  console.log(wrapMessage)
  console.log(message)
};


module.exports = {
  logger,
  messageLogger
};
