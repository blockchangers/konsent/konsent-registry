const util                      = require('ethereumjs-util');
const { compose }               = require('ramda');

const isValidatePublicKey       = compose(util.isValidPublic, util.toBuffer);
const isValidOrganizationNumber = compose((number) => { number.length === 9});

module.exports = {
  isValidatePublicKey,
  isValidOrganizationNumber
};
