# Konsent Registry

Simple "REST-API" for adding and listing members of the konsent network.

Adding members is only possible if you add an authorization header with an bearer token in the request

``` sh
Authorization: Bearer 123
```

## Requirements

* [NodeJs](http://nodejs.org) >= 10.8.x
* [mongodb](http://mongodb.org)

## Install / Run (localhost)

```sh
$ docker run -d -p 27017:27017 -v ~/data:/data/db mongo
$ git clone git@gitlab.com:blockchangers/konsent/konsent-registry.git
$ npm install
```

```sh
$ npm run dev
```

[http://localhost:8082/](http://localhost:8082/)


## Deployment
An Continuous Integration Pipeline is configured for you on GitLab.
You can inspect the configuration and triggers in the following file

```
.gitlab-ci.yml
```


Pushing on staging branch will deploy it to staging environment on heroku.

Pushing on master branch will deploy it
straight to production.

## Hosting

We host the application on Heroku (Free)

The application will go to sleep after 30 minutes if not used.

Production URL [https://konsent-registry.herokuapp.com](https://konsent-registry.herokuapp.com)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]


## Environments
Configuration for the different environments.

```
config/env
├── development.js
├── production.js
└── test.js
```

* DB: 'mongodb://localhost:27017organizations',
* GOLDEN_TOKEN: '123'

DB
> points to where you want to store your data about organizations

GOLDEN_TOKEN
> the authorization token needed to persist data.

## Database
Models are persisted in mongodb , both for localhost / and production.

## Logging
Logging statements in production is redirect to LogDNA.

## Analytics
Data from the application is sent to NewRelic. (Errord, Response times)


## FAQ

## License

MIT
