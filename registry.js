require('newrelic');
require('dotenv').config();

const fs        = require('fs');
const join      = require('path').join;
const express   = require('express');
const mongoose  = require('mongoose');
const passport  = require('passport');
const config    = require('./config');

const models = join(__dirname, 'app/models');
const port = process.env.PORT || 8082;
const app = express();

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^\.].*\.js$/))
  .forEach(file => require(join(models, file)));

// Bootstrap routes
require('./config/passport')(passport);
require('./config/express')(app, passport);
require('./config/routes')(app, passport);

const logo = `
██▀███  ▓█████   ▄████  ██▓  ██████ ▄▄▄█████▓ ██▀███ ▓██   ██▓
▓██ ▒ ██▒▓█   ▀  ██▒ ▀█▒▓██▒▒██    ▒ ▓  ██▒ ▓▒▓██ ▒ ██▒▒██  ██▒
▓██ ░▄█ ▒▒███   ▒██░▄▄▄░▒██▒░ ▓██▄   ▒ ▓██░ ▒░▓██ ░▄█ ▒ ▒██ ██░
▒██▀▀█▄  ▒▓█  ▄ ░▓█  ██▓░██░  ▒   ██▒░ ▓██▓ ░ ▒██▀▀█▄   ░ ▐██▓░
░██▓ ▒██▒░▒████▒░▒▓███▀▒░██░▒██████▒▒  ▒██▒ ░ ░██▓ ▒██▒ ░ ██▒▓░
░ ▒▓ ░▒▓░░░ ▒░ ░ ░▒   ▒ ░▓  ▒ ▒▓▒ ▒ ░  ▒ ░░   ░ ▒▓ ░▒▓░  ██▒▒▒
  ░▒ ░ ▒░ ░ ░  ░  ░   ░  ▒ ░░ ░▒  ░ ░    ░      ░▒ ░ ▒░▓██ ░▒░
  ░░   ░    ░   ░ ░   ░  ▒ ░░  ░  ░    ░        ░░   ░ ▒ ▒ ░░
   ░        ░  ░      ░  ░        ░              ░     ░ ░
                                                       ░ ░
`;

const connect = () => {
  mongoose.connect(config.DB, {
    useNewUrlParser: true,
    keepAlive: true,
    reconnectTries: 30
  });
  return mongoose.connection;
};

const listen = () => {
  if (app.get('env') === 'test') return;
  app.listen(port);
  console.log('Express app started on port ' + port);
  console.log(logo);
};

const db = connect();

db.on('error', console.error.bind(console, 'connection error:'));
db.on('disconnected', connect);
db.once('open', listen);

module.exports = app;
