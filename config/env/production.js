module.exports = {
  DB: process.env.MONGODB_URI,
  GOLDEN_TOKEN: process.env.GOLDEN_TOKEN
};
