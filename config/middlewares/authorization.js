module.exports = (passport) => {
  return (passport.authenticate('bearer', { session: false }));
}
