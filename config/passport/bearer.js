const Strategy = require('passport-http-bearer').Strategy;
const config    = require('../../config');
const GOLDEN_TOKEN = config.GOLDEN_TOKEN;

module.exports = new Strategy((token, cb) => {
  if(token === GOLDEN_TOKEN) {
    return cb(null, token);
  } else {
    logger.info(`invalid token request: ${token}`);
    return cb(null, false, { message: 'Invalid Token' });
  }
});
