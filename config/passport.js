const bearer = require('./passport/bearer');

module.exports = (passport) => {
  passport.use(bearer);
};


