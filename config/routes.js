const swaggerUi         = require('swagger-ui-express');
const swaggerDocument   = require('../swagger.json');
const organization      = require('../app/controllers/organization');

module.exports = (app, passport) => {
  app.get('/api/v1/organizations', organization.getAll);
  app.get('/api/v1/organizations/:did', organization.getSingle);
  app.delete('/api/v1/organizations/:did',  require('./middlewares/authorization')(passport), organization.deleteSingle);
  app.post('/api/v1/organizations', require('./middlewares/authorization')(passport), organization.post);
  app.use('/api/v1/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  app.use((err, req, res, next) => {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    };

    console.error(err.stack);

    if (err.stack.includes('ValidationError')) {
      res.status(422).render('422', { error: err.stack });
      return;
    };

    // error page
    res.status(500).render('500', { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use((req, res) => {
    const payload = {
      url: req.originalUrl,
      error: 'Not found'
    };
    if (req.accepts('json')) return res.status(404).json(payload);
    res.status(404).render('404', payload);
  });
};
