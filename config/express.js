const cors                            = require('cors');
const morgan                          = require('morgan');
const bodyParser                      = require('body-parser');
const compression                     = require('compression');
const helmet                          = require('helmet');
const methodOverride                  = require('method-override');


module.exports = (app, passport) => {
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(compression());
  app.use(cors());
  app.use(morgan('dev'));
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.set('trust proxy', true);
};
